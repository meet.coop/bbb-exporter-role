# bbb-exporter

Installs and configures bbb-exporter Prometheus exporter for BigBlueButton. Uses the Docker install method as per https://bigbluebutton-exporter.greenstatic.dev/installation/bigbluebutton_exporter/
